from StroyBot import database
import csv


ingeneer = database.Ingeneer("sqlite:///db.sqlite")
FILE_NAME = 'journal.csv'

def create_journal(building_name):
    works = ingeneer.get_building_works(building_name)
    pos = 0

    with open(FILE_NAME, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=';')
        writer.writerow([
            '№№ п/п', 
            'Дата выполнения работ', 
            'Наименование работ, выполняемых в процессе строительства, ректонтрукции, капитального ремонта объекта капитального строительства',
            'Должность, фамилия, инициалы, уполномоченного представителя лица, осуществляющего строительство'
            ])
        for work in works:
            pos += 1
            writer.writerow([pos, work[0], work[1], f'{work[2]} {work[3]}'])
            print(work)
    return FILE_NAME
