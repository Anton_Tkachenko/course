import telebot
from telebot import types
import json
import os.path
import datetime
from StroyBot import database
from StroyBot import journal_csv


TOKEN = "5146798523:AAFR1jI9mLYeyiOYEBUdLJ8nOYwWxen_p9o"
ADMIN_ID = '1550981910'
GROUP_ID = int(-782119105)
bot = telebot.TeleBot(TOKEN)

director = database.Director("sqlite:///db.sqlite")
ingeneer = database.Ingeneer("sqlite:///db.sqlite")

active_report = False # Флаг, о том, что активен режим заполнения отчета
buildings = ingeneer.get_buildings()
work = {}
new_building = {}
new_employee = {}
report = ''


if os.path.exists('users.json'):
    with open('users.json') as f:
        users = json.load(f)
else:
    users = {ADMIN_ID: 'admin'}

def work_init():
   return {'building_short_name' : None, 'date' : None, 'responsible' : None, 'headcount' : None, 'report' : None}

def new_building_init():
    return {'building_full_name' : None, 'building_short_name' : None}

def get_employees():
    pass

#Приветсвенный блок функций. Связка сотрудника с ID в Телеграм.
#Сохранение информации о пользователях в файл
@bot.message_handler(commands=['start'])
def start_message(message):
    if str(message.chat.id) not in users:
        employees = get_employees()
        markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
        buttons = []
        for employee in employees:
            buttons.append(types.KeyboardButton(employee))
        markup.add(*buttons)
        msg = bot.send_message(message.chat.id, 'Привет, давай знакомиться. Как тебя зовут?', reply_markup=markup)
        bot.register_next_step_handler(msg, user_name_step)
    else:
        bot.send_message(message.chat.id, f'Привет,  {users[str(message.chat.id)]}')

def user_name_step(message):
    username = message.text
    users[str(message.chat.id)] = username
    bot.send_message(message.chat.id, f'Привет, {username}, рад знакомству!')
    with open('users.json', 'w') as f:
        print('Запись пользователей в файл')
        json.dump(users, f)
    

# Блок функций, отвечающих за вввод отчетов
@bot.message_handler(commands=['отчет', 'Отчет'])
def start_report(message):
    print('Обрабатываем отчет')
    active_report = True
    work['responsible'] = users[str(message.chat.id)]
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    buttons = []
    for building in buildings:
        buttons.append(types.KeyboardButton(building))
    markup.add(*buttons)
    msg = bot.send_message(message.chat.id, "Выбери объект",  reply_markup=markup)
    bot.register_next_step_handler(msg, building_name_step)


def building_name_step(message):
    print('Обрабатываем название объекта')
    work['building_short_name'] = message.text
    
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    today = datetime.date.today()
    yesturday = datetime.date.today() - datetime.timedelta(days=1)
    print(today, yesturday)
    button_today = types.KeyboardButton('Сегодня')
    button_yesturday = types.KeyboardButton('Вчера')
    
    markup.add(button_today, button_yesturday)
    msg = bot.send_message(message.chat.id, "За какой день заполняем отчет?", reply_markup=markup)
    bot.register_next_step_handler(msg, date_step)

def date_step(message):
    if message.text == "Сегодня":
        work['date'] = datetime.date.today()
    else: 
        work['date'] = datetime.date.today() - datetime.timedelta(days=1)
    msg = bot.send_message(message.chat.id, "Сколько человек было задействовано?")
    bot.register_next_step_handler(msg, counthead_step)

def counthead_step(message):
    text = message.text
    if not text.isdigit():
        msg = bot.send_message(message.chat.id, 'Количество должно быть числом, введите ещё раз.')
        bot.register_next_step_handler(msg, counthead_step)
        return
    work['headcount'] = int(text)
    msg = bot.send_message(message.chat.id, "Что было выполнено за день?")
    bot.register_next_step_handler(msg, report_step)

def report_step(message):
    work['report'] = message.text
    report = f'''
    {work['date']},
    {work['building_short_name']},
    {work['report']},
    Персонал {work['headcount']} человек,
    Ответственный {work['responsible']}.
    '''
    button_yes = types.KeyboardButton('Да')
    button_no = types.KeyboardButton('Нет')
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.add(button_yes, button_no)
    msg = bot.send_message(message.chat.id, f"Сохранить отчет \n {report} ?", reply_markup=markup)
    bot.register_next_step_handler(msg, save_report_step)
 
def save_report_step(message):
    if message.text == 'Да':
        date = f"{work['date'].day}.{work['date'].month}.{work['date'].year}"
        report = f'''
        {date}
        {work['building_short_name']},
        {work['report']},
        Персонал: {work['headcount']} человек,
        Ответственный: {work['responsible']}.
    '''
        try:
            ingeneer.add_report(work['building_short_name'], work['responsible'], work['headcount'], work['report'], work['date'])
            bot.send_message(GROUP_ID, report)
        except:
            bot.send_message(message.chat.id, 'Что-то пошло не так, отчет не сохранен!')
        

#Блок функций, отвечающих за вввод новых объектов
@bot.message_handler(commands=['объект', 'Объект'])
def start_report(message):
    print('Обрабатываем ввод нового объекта')
    msg = bot.send_message(message.chat.id, "Введите полное наименование объекта согласно договору")
    bot.register_next_step_handler(msg, building_full_name_step)

def building_full_name_step(message):
    new_building['building_full_name'] = message.text
    msg = bot.send_message(message.chat.id, "Введите краткое наименование объекта")
    bot.register_next_step_handler(msg, building_short_name_step)

def building_short_name_step(message):
    new_building['building_short_name'] = message.text
    button_yes = types.KeyboardButton('Да')
    button_no = types.KeyboardButton('Нет')
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.add(button_yes, button_no)
    msg = bot.send_message(message.chat.id, f"Сохранить новый объект \n {new_building} ?", reply_markup=markup)
    bot.register_next_step_handler(msg, save_new_building)

def save_new_building(message):
    if message.text == 'Да':
        try:
            director.add_building(new_building['building_full_name'], new_building['building_short_name'])
            buildings.append(new_building['building_short_name'])
        except:
            bot.send_message(message.chat.id, 'Что-то пошло не так')


#Блок функций, отвечающих за вввод новых сотрудников

@bot.message_handler(commands=['сотрудник', 'Сотрудник'])
def start_report(message):
    print('Обрабатываем ввод нового сотрудника')
    msg = bot.send_message(message.chat.id, "Введите полное имя сотрудника")
    bot.register_next_step_handler(msg, employee_name_step)

def employee_name_step(message):
    new_employee['name'] = message.text
    msg = bot.send_message(message.chat.id, "Введите должность")
    bot.register_next_step_handler(msg, position_step)

def position_step(message):
    new_employee['position'] = message.text
    button_yes = types.KeyboardButton('Да')
    button_no = types.KeyboardButton('Нет')
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.add(button_yes, button_no)
    msg = bot.send_message(message.chat.id, 
                           f"Сохранить данные о сотруднике \n {new_employee['position']} {new_employee['name']}?", 
                           reply_markup=markup)
    bot.register_next_step_handler(msg, save_new_employee)

def save_new_employee(message):
    if message.text == 'Да':
        try:
            director.add_employee(new_employee['name'], new_employee['position'])
        except:
            bot.send_message(message.chat.id, 'Что-то пошло не так')


@bot.message_handler(commands=['журнал', 'Журнал'])
def start_report(message):
    print('Обрабатываем заявку на загрузку журнала работ по объеку')
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    buttons = []
    for building in buildings:
        buttons.append(types.KeyboardButton(building))
    markup.add(*buttons)
    msg = bot.send_message(message.chat.id, "Выбери объект",  reply_markup=markup)
    bot.register_next_step_handler(msg, building_journal_step)

def building_journal_step(message):
    file_name = journal_csv.create_journal(message.text)
    print(file_name)
    with open(file_name, 'rb') as journal:
        bot.send_document(message.chat.id, journal)


@bot.message_handler(commands=['stop'])
def stop_bot(message):
    print('Останавливаем работу бота')
    bot.stop_polling()
    bot.send_photo(message.chat.id, 'AgACAgIAAxkBAAICWWI8dtLX3ZM_Urz1eqPJJaiiRfIXAAJcuTEbINfhScQpW6ZI2KPwAQADAgADeQADIwQ')

@bot.message_handler(commands=['photo'])
def print_id(message):
    msg = bot.send_message(message.chat.id, "Начинаем загрузку изображений!!!")
    bot.register_next_step_handler(msg, image_step)

def image_step(message):
    idphoto = message.photo[-1].file_id
    print('Тестовый обработчик событий',message.chat.id, idphoto)
    msg = bot.send_photo(message.chat.id, idphoto)
