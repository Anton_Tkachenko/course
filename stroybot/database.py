import datetime
import sqlalchemy as sa
from sqlalchemy.orm import declarative_base, Session


Base = declarative_base()


class TimestampModelMixin:
    created_at = sa.Column(sa.DateTime, default=datetime.datetime.now)


class PrimaryKeyMixin:
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)


# Класс, описывающий объекты.
# id - первичный ключ
# full_name Полное наименование объекта (из договора)
# short_name - краткое наименование объекта (обиходное) для вывода ежедневного отчета
# is_active - статус стпройки: действующий или архивный.
class Building(PrimaryKeyMixin, Base):
    __tablename__ = "buildings"

    full_name = sa.Column(sa.Unicode(64), nullable=False, unique=True)
    short_name = sa.Column(sa.Unicode(64), nullable=False, unique=True)
    is_active = sa.Column(sa.Boolean, nullable=False, default=True)


# Класс, описывающий таблицу c сотрудниками:
# id - первичный ключ
# name - ФИО сотрудника
# position - должность. Должности на данном этапе разработки вводим вручную
# is_active - статус сотрудника, работает или уволен
class Employee(PrimaryKeyMixin, Base):
    __tablename__ = "employees"

    name = sa.Column(sa.Unicode(64), nullable=False) 
    position = sa.Column(sa.Unicode(64), nullable=False) 
    is_active = sa.Column(sa.Boolean, nullable=False, default=True)


# Класс, описывающий выполненные работы
# id - первичный ключ
# created_at - дата выполнения работ, по умолчанию текущая дата
# building_id - внешний ключ, объект, на котором выполнялись работы
# responsible - внешний ключ, ответственный за производство работ
# report - отчет о выполненных работах
# headcount - численность персонала, задействованных в работах
# photo - фотоотчет о выполненных работах (ссылка на папку с фотографиями?)
class Work(PrimaryKeyMixin, TimestampModelMixin, Base):
    __tablename__ = "works"

    building_id = sa.Column(sa.ForeignKey(f"{Building.__tablename__}.id"), nullable=False)
    responsible = sa.Column(sa.ForeignKey(f"{Employee.__tablename__}.id"), nullable=False)
    headcount = sa.Column(sa.Integer, sa.CheckConstraint("headcount > 0"), nullable=False)
    report = sa.Column(sa.Unicode(64), nullable=False)
    # photo = sa.Column(sa.Unicode(64), nullable=False)


# Описание ролей в приложении
class Role:
    def __init__(self, dsn: str):
        engine = sa.create_engine(dsn)
        Base.metadata.create_all(engine)
        self.session = Session(engine)

# Класс, описывающий роли руководителя:
# Полномочия руководителя:
# - добавляет новые объекты
# - меняет статус объекта - в перспективе
# - добавляет новых сотрудников
# - меняет статус сотрудников - в перспективе
class Director(Role):

    #Добавление нового сотрудника
    def add_employee(self, name: str, position: str)->Employee:
        employee = Employee(name=name, position=position)
        with self.session:
            self.session.add(employee)
            self.session.commit()
        return employee

    #Добавление объекта строительства
    def add_building(self, full_name: str, short_name: str)->Building:
        print('Добавляем объект в базу данных')
        building = Building(full_name=full_name, short_name=short_name)
        with self.session:
            self.session.add(building)
            self.session.commit()
        print(f'Добавили объект {building} в базу данных')
        return building

# Класс, описывающий роли инженера:
# Возможности инженера:
# - добавляет ежедневные отчеты
# - получает журнал работ по выбранному объекту

class Ingeneer(Role):

    def add_report(self, building_name: str, responsible: str, headcount: int, report: str, date: datetime.date) -> Work:
        building_id = self.get_building_id(building_name)
        employee_id = self.get_employee_id(responsible)
        work = Work(building_id =building_id, responsible=employee_id, headcount=headcount, report=report, created_at=date)
        with self.session:
            self.session.add(work)
            self.session.commit()
        return work


    def get_employee_id(self, employee_name: str) -> int:
        with self.session:
            id = self.session.query(Employee).filter(Employee.name == employee_name).all()
        return id[0].id    

    def get_employee(self, id: int) -> int:
        with self.session:
            employee = self.session.query(Employee).filter(Employee.id == id).all()
        return employee[0].position, employee[0].name


    def get_building_id(self, building_name) -> int:
        with self.session:
            id = self.session.query(Building).filter(Building.short_name == building_name).all()
        return id[0].id  

    def get_buildings(self) -> list:
        result = []
        with self.session:
            buildings = self.session.query(Building).filter(Building.is_active).all()
        for building in buildings:
            result.append(building.short_name)
        return result
    
    def get_building_works(self, building_name):
        result = []
        building_id = self.get_building_id(building_name)
        works = self.session.query(Work).filter(Work.building_id == building_id).all()
        for work in works:
            result.append((work.created_at.date().strftime('%d.%m.%Y'), work.report, *self.get_employee(work.responsible)))
        return result
        

